package com.sd.selenium;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class App {
	public static void main(String[] args) {
		String os = System.getProperty("os.name");
		System.out.println(os);
		if (os.toLowerCase().contains("windows"))
			System.setProperty("webdriver.chrome.driver", "src/resources/win/chromedriver.exe");
		else
			System.setProperty("webdriver.chrome.driver", "src/resources/linux/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		String baseUrl = "https://www.tutorialspoint.com/index.htm";
		String expectedTitle = "You are browsing the best resource for ";
		driver.get(baseUrl);
		WebElement element = driver.findElement(By.xpath("//h4"));

		if (element.getText().contains(expectedTitle)) {
			System.out.println("Test Passed!");
		} else {
			System.out.println("Test Failed");
		}
		driver.close();
	}
}
